import dao.Deliveries;
import dao.EconomyResult;
import dao.FieldFirstResult;
import dao.HighestNRRResult;
import dao.Matches;
import dao.TotalScoresResult;
import queries.BestEconomyQuery;
import queries.FieldFirstQuery;
import queries.HighestNRR;
import queries.TotalScores;
import reader.CSVReader;
import reader.DeliveriesReader;
import reader.InfoReader;
import reader.MatchesReader;

import java.util.List;

public class Starter {

    public static void main(String args[]) {
        CSVReader csvReader = new CSVReader();
        InfoReader<Matches> matchesReader = new MatchesReader(csvReader);
        InfoReader<Deliveries> deliveriesReader = new DeliveriesReader(csvReader);

        List<Matches> matches = matchesReader.getInfo();
        List<Deliveries> deliveries = deliveriesReader.getInfo();

        List<FieldFirstResult> fieldFirstResults = new FieldFirstQuery(matches).getResult(4);
        List<TotalScoresResult> totalScoresResults = new TotalScores(deliveries, matches).getResult();
        List<EconomyResult> bestEconomyResults = new BestEconomyQuery(deliveries, matches).getResult();
        List<HighestNRRResult> highestNRRResults = new HighestNRR(deliveries, matches).getResult();

        System.out.println(fieldFirstResults);
        System.out.println(totalScoresResults);
        System.out.println(bestEconomyResults);
        System.out.println(highestNRRResults);
    }
}
