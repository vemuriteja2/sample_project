package dao;

public class EconomyResult {

    private String year;
    private String bowler;
    private double economy;

    @Override
    public String toString() {
        return "EconomyResult{" + "year='" + year + '\'' + ", bowler='" + bowler + '\'' + ", economy=" + economy + '}';
    }

    public String getYear() {
        return year;
    }

    public void setYear(final String year) {
        this.year = year;
    }

    public String getBowler() {
        return bowler;
    }

    public void setBowler(final String bowler) {
        this.bowler = bowler;
    }

    public double getEconomy() {
        return economy;
    }

    public void setEconomy(final double economy) {
        this.economy = economy;
    }
}
