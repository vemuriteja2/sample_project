package dao;

public class FieldFirstResult {

    private String year;
    private String team;
    private int count;

    public String getTeam() {
        return team;
    }

    public void setTeam(final String team) {
        this.team = team;
    }

    public int getCount() {
        return count;
    }

    public void setCount(final int count) {
        this.count = count;
    }

    public String getYear() {

        return year;
    }

    public void setYear(final String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "FieldFirstResult{" + "year='" + year + '\'' + ", team='" + team + '\'' + ", count=" + count + '}';
    }
}
