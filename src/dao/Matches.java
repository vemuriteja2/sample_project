package dao;

public class Matches {

    private int MATCH_ID;
    private int SEASON;
    private String CITY;
    private String DATE;
    private String TEAM1;
    private String TEAM2;
    private String TOSS_WINNER;
    private String TOSS_DECISION;
    private String RESULT;
    private String WINNER;

    @Override
    public String toString() {
        return "Matches{"
               + "MATCH_ID="
               + MATCH_ID
               + ", SEASON="
               + SEASON
               + ", CITY='"
               + CITY
               + '\''
               + ", DATE='"
               + DATE
               + '\''
               + ", TEAM1='"
               + TEAM1
               + '\''
               + ", TEAM2='"
               + TEAM2
               + '\''
               + ", TOSS_WINNER='"
               + TOSS_WINNER
               + '\''
               + ", TOSS_DECISION='"
               + TOSS_DECISION
               + '\''
               + ", RESULT='"
               + RESULT
               + '\''
               + ", WINNER='"
               + WINNER
               + '\''
               + '}';
    }

    public static Matches fromStringArray(final String[] matchInfo) {
        /*if (matchInfo.length != 10) {
            throw new IllegalArgumentException("Row Info not matching Matches object size");
        }*/
        Matches matches = new Matches();
        matches.setMATCH_ID(Integer.parseInt(matchInfo[0]));
        matches.setSEASON(Integer.parseInt(matchInfo[1]));
        matches.setCITY(matchInfo[2]);
        matches.setDATE(matchInfo[3]);
        matches.setTEAM1(matchInfo[4]);
        matches.setTEAM2(matchInfo[5]);
        matches.setTOSS_WINNER(matchInfo[6]);
        matches.setTOSS_DECISION(matchInfo[7]);
        matches.setRESULT(matchInfo[8]);
        matches.setWINNER(matchInfo.length > 9 ? matchInfo[9] : "");
        return matches;
    }

    public int getMATCH_ID() {
        return MATCH_ID;
    }

    public void setMATCH_ID(final int MATCH_ID) {
        this.MATCH_ID = MATCH_ID;
    }

    public int getSEASON() {
        return SEASON;
    }

    public void setSEASON(final int SEASON) {
        this.SEASON = SEASON;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(final String CITY) {
        this.CITY = CITY;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(final String DATE) {
        this.DATE = DATE;
    }

    public String getTEAM1() {
        return TEAM1;
    }

    public void setTEAM1(final String TEAM1) {
        this.TEAM1 = TEAM1;
    }

    public String getTEAM2() {
        return TEAM2;
    }

    public void setTEAM2(final String TEAM2) {
        this.TEAM2 = TEAM2;
    }

    public String getTOSS_WINNER() {
        return TOSS_WINNER;
    }

    public void setTOSS_WINNER(final String TOSS_WINNER) {
        this.TOSS_WINNER = TOSS_WINNER;
    }

    public String getTOSS_DECISION() {
        return TOSS_DECISION;
    }

    public void setTOSS_DECISION(final String TOSS_DECISION) {
        this.TOSS_DECISION = TOSS_DECISION;
    }

    public String getRESULT() {
        return RESULT;
    }

    public void setRESULT(final String RESULT) {
        this.RESULT = RESULT;
    }

    public String getWINNER() {
        return WINNER;
    }

    public void setWINNER(final String WINNER) {
        this.WINNER = WINNER;
    }
}
