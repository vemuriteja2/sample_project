package dao;

public class TotalScoresResult {

    private String year;
    private String teamName;
    private int foursCount;
    private int sixesCount;
    private int totalScore;

    public String getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "TotalScoresResult{"
               + "year='"
               + year
               + '\''
               + ", teamName='"
               + teamName
               + '\''
               + ", foursCount="
               + foursCount
               + ", sixesCount="
               + sixesCount
               + ", totalScore="
               + totalScore
               + '}';
    }

    public void setYear(final String year) {
        this.year = year;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(final String teamName) {
        this.teamName = teamName;
    }

    public int getFoursCount() {
        return foursCount;
    }

    public void setFoursCount(final int foursCount) {
        this.foursCount = foursCount;
    }

    public int getSixesCount() {
        return sixesCount;
    }

    public void setSixesCount(final int sixesCount) {
        this.sixesCount = sixesCount;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(final int totalScore) {
        this.totalScore = totalScore;
    }

}
