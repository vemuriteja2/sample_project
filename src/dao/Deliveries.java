package dao;

public class Deliveries {

    private int MATCH_ID;
    private int INNING;
    private String BATTING_TEAM;
    private String BOWLING_TEAM;
    private int OVER;
    private int BALL;
    private String BATSMAN;
    private String BOWLER;
    private int WIDE_RUNS;
    private int BYE_RUNS;
    private int LEGBYE_RUNS;
    private int NOBALL_RUNS;
    private int PENALTY_RUNS;
    private int BATSMAN_RUNS;
    private int EXTRA_RUNS;
    private int TOTAL_RUNS;

    public static Deliveries fromStringArray(final String[] deliveryInfo) {
        Deliveries de = new Deliveries();
        if (deliveryInfo.length != 16) {
            throw new IllegalArgumentException("RowInfo is not equal to the size of Deliveries object");
        }
        de.setMATCH_ID(Integer.parseInt(deliveryInfo[0]));
        de.setINNING(Integer.parseInt(deliveryInfo[1]));
        de.setBATTING_TEAM(deliveryInfo[2]);
        de.setBOWLING_TEAM(deliveryInfo[3]);
        de.setOVER(Integer.parseInt(deliveryInfo[4]));
        de.setBALL(Integer.parseInt(deliveryInfo[5]));
        de.setBATSMAN(deliveryInfo[6]);
        de.setBOWLER(deliveryInfo[7]);
        de.setWIDE_RUNS(Integer.parseInt(deliveryInfo[8]));
        de.setBYE_RUNS(Integer.parseInt(deliveryInfo[9]));
        de.setLEGBYE_RUNS(Integer.parseInt(deliveryInfo[10]));
        de.setNOBALL_RUNS(Integer.parseInt(deliveryInfo[11]));
        de.setPENALTY_RUNS(Integer.parseInt(deliveryInfo[12]));
        de.setBATSMAN_RUNS(Integer.parseInt(deliveryInfo[13]));
        de.setEXTRA_RUNS(Integer.parseInt(deliveryInfo[14]));
        de.setTOTAL_RUNS(Integer.parseInt(deliveryInfo[15]));
        return de;
    }

    public int getMATCH_ID() {
        return MATCH_ID;
    }

    public void setMATCH_ID(final int MATCH_ID) {
        this.MATCH_ID = MATCH_ID;
    }

    public int getINNING() {
        return INNING;
    }

    public void setINNING(final int INNING) {
        this.INNING = INNING;
    }

    public String getBATTING_TEAM() {
        return BATTING_TEAM;
    }

    public void setBATTING_TEAM(final String BATTING_TEAM) {
        this.BATTING_TEAM = BATTING_TEAM;
    }

    public String getBOWLING_TEAM() {
        return BOWLING_TEAM;
    }

    public void setBOWLING_TEAM(final String BOWLING_TEAM) {
        this.BOWLING_TEAM = BOWLING_TEAM;
    }

    public int getOVER() {
        return OVER;
    }

    public void setOVER(final int OVER) {
        this.OVER = OVER;
    }

    public int getBALL() {
        return BALL;
    }

    public void setBALL(final int BALL) {
        this.BALL = BALL;
    }

    public String getBATSMAN() {
        return BATSMAN;
    }

    public void setBATSMAN(final String BATSMAN) {
        this.BATSMAN = BATSMAN;
    }

    public String getBOWLER() {
        return BOWLER;
    }

    public void setBOWLER(final String BOWLER) {
        this.BOWLER = BOWLER;
    }

    public int getWIDE_RUNS() {
        return WIDE_RUNS;
    }

    public void setWIDE_RUNS(final int WIDE_RUNS) {
        this.WIDE_RUNS = WIDE_RUNS;
    }

    public int getBYE_RUNS() {
        return BYE_RUNS;
    }

    public void setBYE_RUNS(final int BYE_RUNS) {
        this.BYE_RUNS = BYE_RUNS;
    }

    public int getLEGBYE_RUNS() {
        return LEGBYE_RUNS;
    }

    public void setLEGBYE_RUNS(final int LEGBYE_RUNS) {
        this.LEGBYE_RUNS = LEGBYE_RUNS;
    }

    public int getNOBALL_RUNS() {
        return NOBALL_RUNS;
    }

    public void setNOBALL_RUNS(final int NOBALL_RUNS) {
        this.NOBALL_RUNS = NOBALL_RUNS;
    }

    public int getPENALTY_RUNS() {
        return PENALTY_RUNS;
    }

    public void setPENALTY_RUNS(final int PENALTY_RUNS) {
        this.PENALTY_RUNS = PENALTY_RUNS;
    }

    public int getBATSMAN_RUNS() {
        return BATSMAN_RUNS;
    }

    public void setBATSMAN_RUNS(final int BATSMAN_RUNS) {
        this.BATSMAN_RUNS = BATSMAN_RUNS;
    }

    public int getEXTRA_RUNS() {
        return EXTRA_RUNS;
    }

    public void setEXTRA_RUNS(final int EXTRA_RUNS) {
        this.EXTRA_RUNS = EXTRA_RUNS;
    }

    public int getTOTAL_RUNS() {
        return TOTAL_RUNS;
    }

    public void setTOTAL_RUNS(final int TOTAL_RUNS) {
        this.TOTAL_RUNS = TOTAL_RUNS;
    }
}
