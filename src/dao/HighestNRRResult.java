package dao;

public class HighestNRRResult {

    private String year;
    private String teamName;
    private double nrr;

    @Override
    public String toString() {
        return "HighestNRRResult{" + "year='" + year + '\'' + ", teamName='" + teamName + '\'' + ", nrr=" + nrr + '}';
    }

    public String getYear() {
        return year;
    }

    public void setYear(final String year) {
        this.year = year;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(final String teamName) {
        this.teamName = teamName;
    }

    public double getNrr() {
        return nrr;
    }

    public void setNrr(final double nrr) {
        this.nrr = nrr;
    }
}
