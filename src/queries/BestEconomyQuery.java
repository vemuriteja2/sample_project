package queries;

import dao.Deliveries;
import dao.EconomyResult;
import dao.Matches;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class EconomyStructure {

    public int ballsCount;
    public int runsGiven;

    EconomyStructure() {
        ballsCount = 0;
        runsGiven = 0;
    }
}

class SortByEconomy implements Comparator<EconomyResult> {

    @Override
    public int compare(final EconomyResult o1, final EconomyResult o2) {
        return Double.compare(o1.getEconomy(), o2.getEconomy());
    }
}

public class BestEconomyQuery {

    private static final String KEY_DELIMITER = "::";
    private final List<Deliveries> deliveries;
    private final List<Matches> matches;
    private final Map<String, EconomyStructure> counter;
    private final Map<Integer, Integer> matchSeasonMap;

    public BestEconomyQuery(final List<Deliveries> deliveriesList, final List<Matches> matches) {
        deliveries = deliveriesList;
        this.matches = matches;
        counter = new HashMap<>();
        matchSeasonMap = new HashMap<>();
        for (int i = 0; i < matches.size(); i++) {
            matchSeasonMap.put(matches.get(i)
                                      .getMATCH_ID(),
                               matches.get(i)
                                      .getSEASON());
        }
    }

    public List<EconomyResult> getResult() {
        List<EconomyResult> result = aggregate();
        Collections.sort(result, new SortByEconomy());
        return result;
    }

    private List<EconomyResult> aggregate() {
        for (int row = 0; row < deliveries.size(); row++) {
            String key = generateKey(deliveries.get(row));
            EconomyStructure value = counter.getOrDefault(key, new EconomyStructure());
            value.ballsCount = value.ballsCount + (deliveries.get(row)
                                                             .getEXTRA_RUNS() == 0 ? 1 : 0);
            value.runsGiven = value.runsGiven + (deliveries.get(row)
                                                           .getBATSMAN_RUNS() + deliveries.get(row)
                                                                                          .getEXTRA_RUNS() + deliveries.get(row)
                                                                                                                       .getPENALTY_RUNS());
            counter.put(key, value);
        }
        return counter.entrySet()
                      .stream()
                      .map(elem -> getEconomyResult(elem))
                      .collect(Collectors.toList());
    }

    private EconomyResult getEconomyResult(final Map.Entry<String, EconomyStructure> elem) {
        EconomyResult result = new EconomyResult();
        double overs = (elem.getValue().ballsCount / 6);
        String[] keys = elem.getKey()
                            .split(KEY_DELIMITER);
        result.setYear(keys[0]);
        result.setBowler(keys[1]);
        if (overs >= 10) {
            result.setEconomy(elem.getValue().runsGiven / overs);
        } else {
            result.setEconomy(Double.MAX_VALUE);
        }
        return result;
    }

    private String generateKey(final Deliveries deliveries) {
        return matchSeasonMap.get(deliveries.getMATCH_ID()) + KEY_DELIMITER + deliveries.getBOWLER();
    }

}
