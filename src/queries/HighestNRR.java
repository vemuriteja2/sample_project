package queries;

import dao.Deliveries;
import dao.HighestNRRResult;
import dao.Matches;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class NrrStructure {

    public String team;
    public double totalRunsScored;
    public double totalRunsGiven;
    public double totalBallsBowled;
    public double totalBallsFaced;

    NrrStructure() {
        team = "";
        totalRunsScored = 0;
        totalRunsGiven = 0;
        totalBallsFaced = 0;
        totalBallsBowled = 0;
    }

    @Override
    public String toString() {
        return "NrrStructure{"
               + "team='"
               + team
               + '\''
               + ", totalRunsScored="
               + totalRunsScored
               + ", totalRunsGiven="
               + totalRunsGiven
               + ", totalBallsBowled="
               + totalBallsBowled
               + ", totalBallsFaced="
               + totalBallsFaced
               + '}';
    }
}

class SortByHighestNrr implements Comparator<HighestNRRResult> {

    @Override
    public int compare(final HighestNRRResult o1, final HighestNRRResult o2) {
        return Double.compare(o2.getNrr(), o1.getNrr());
    }
}

public class HighestNRR {

    private static final String KEY_DELIMITER = "::";
    private final List<Deliveries> deliveries;
    private final List<Matches> matches;
    private final Map<String, NrrStructure> counter;
    private final Map<Integer, Integer> matchSeasonMap;

    public HighestNRR(final List<Deliveries> deliveriesList, final List<Matches> matches) {
        deliveries = deliveriesList;
        this.matches = matches;
        counter = new HashMap<>();
        matchSeasonMap = new HashMap<>();
        for (int i = 0; i < matches.size(); i++) {
            matchSeasonMap.put(matches.get(i)
                                      .getMATCH_ID(),
                               matches.get(i)
                                      .getSEASON());
        }
    }

    public List<HighestNRRResult> getResult() {
        List<HighestNRRResult> result = aggregate();
        Collections.sort(result, new SortByHighestNrr());
        return result;
    }

    private List<HighestNRRResult> aggregate() {
        for (int row = 0; row < deliveries.size(); row++) {
            String key = generateKey(deliveries.get(row), true);
            NrrStructure value = counter.getOrDefault(key, new NrrStructure());
            value.team = deliveries.get(row)
                                   .getBATTING_TEAM();
            value.totalRunsScored = value.totalRunsScored + deliveries.get(row)
                                                                      .getTOTAL_RUNS();
            value.totalBallsFaced = value.totalBallsFaced + (deliveries.get(row)
                                                                       .getEXTRA_RUNS() == 0 ? 1 : 0);
            counter.put(key, value);
        }
        for (int row = 0; row < deliveries.size(); row++) {
            String key = generateKey(deliveries.get(row), false);
            NrrStructure value = counter.getOrDefault(key, new NrrStructure());
            value.totalRunsGiven = value.totalRunsGiven + deliveries.get(row)
                                                                    .getTOTAL_RUNS();
            value.totalBallsBowled = value.totalBallsBowled + (deliveries.get(row)
                                                                         .getEXTRA_RUNS() == 0 ? 1 : 0);
            counter.put(key, value);
        }
        return counter.entrySet()
                      .stream()
                      .map(elem -> getNrrResult(elem))
                      .collect(Collectors.toList());
    }

    private HighestNRRResult getNrrResult(final Map.Entry<String, NrrStructure> elem) {
        HighestNRRResult result = new HighestNRRResult();
        double nrr = (elem.getValue().totalRunsScored / (elem.getValue().totalBallsFaced / 6)) - (elem.getValue().totalRunsGiven
                                                                                                  / (elem.getValue().totalBallsBowled / 6));
        String[] keys = elem.getKey()
                            .split(KEY_DELIMITER);
        result.setYear(keys[0]);
        result.setTeamName(elem.getValue().team);
        result.setNrr(nrr);
        return result;
    }

    private String generateKey(final Deliveries deliveries, final boolean isBatting) {
        return matchSeasonMap.get(deliveries.getMATCH_ID()) + KEY_DELIMITER + (isBatting ?
                                                                               deliveries.getBATTING_TEAM() :
                                                                               deliveries.getBOWLING_TEAM());
    }

}
