package queries;

import dao.FieldFirstResult;
import dao.Matches;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public class FieldFirstQuery {

    private static final String FIELD_DECISION = "field";
    private static final String KEY_DELIMITER = "::";
    private final List<Matches> matches;
    private final Map<String, Integer> counter;

    public FieldFirstQuery(final List<Matches> matches) {
        this.matches = matches;
        counter = new HashMap<>();
    }

    public List<FieldFirstResult> getResult(final int number) {
        List<Matches> filteredMatches = matches.stream()
                                               .filter(elem -> shouldFiltered(elem, 2016))
                                               .collect(Collectors.toList());
        List<Matches> filteredMatches2017 = matches.stream()
                                                   .filter(elem -> shouldFiltered(elem, 2017))
                                                   .collect(Collectors.toList());
        filteredMatches.addAll(filteredMatches2017);
        return aggregate(filteredMatches, 4);
    }

    private boolean shouldFiltered(final Matches match, final int year) {
        return (match.getSEASON() == year) && match.getTOSS_DECISION()
                                                   .equals(FIELD_DECISION);
    }

    private List<FieldFirstResult> aggregate(List<Matches> data, int limit) {
        for (int row = 0; row < data.size(); row++) {
            String key = generateKey(data.get(row));
            int value = counter.getOrDefault(key, 0);
            value++;
            counter.put(key, value);
        }
        Map<String, Integer> sorted = counter.entrySet()
                                             .stream()
                                             .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                                             .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
        Set<Map.Entry<String, Integer>> allEntries = sorted.entrySet();
        List<FieldFirstResult> result = new ArrayList<>();
        int count = 0;
        for (Map.Entry<String, Integer> entry : allEntries) {
            result.add(getFieldFirstResult(entry));
            count++;
            if (count == limit) {
                break;
            }
        }
        return result;

    }

    private FieldFirstResult getFieldFirstResult(final Map.Entry<String, Integer> elem) {
        FieldFirstResult result = new FieldFirstResult();
        String[] keys = elem.getKey()
                            .split(KEY_DELIMITER);
        result.setYear(keys[0]);
        result.setTeam(keys[1]);
        result.setCount(elem.getValue());
        return result;
    }

    private String generateKey(final Matches matches) {
        return matches.getSEASON() + KEY_DELIMITER + matches.getTOSS_WINNER();
    }
}
