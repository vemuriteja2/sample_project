package queries;

import dao.Deliveries;
import dao.Matches;
import dao.TotalScoresResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Runs {

    public int fours;
    public int sixes;
    public int total;

    Runs() {
        fours = 0;
        sixes = 0;
        total = 0;
    }
}

public class TotalScores {

    private static final String KEY_DELIMITER = "::";
    private final List<Deliveries> deliveries;
    private final List<Matches> matches;
    private final Map<String, Runs> counter;
    private final Map<Integer, Integer> matchSeasonMap;

    public TotalScores(final List<Deliveries> deliveriesList, final List<Matches> matches) {
        deliveries = deliveriesList;
        this.matches = matches;
        counter = new HashMap<>();
        matchSeasonMap = new HashMap<>();
        for (int i = 0; i < matches.size(); i++) {
            matchSeasonMap.put(matches.get(i)
                                      .getMATCH_ID(),
                               matches.get(i)
                                      .getSEASON());
        }
    }

    public List<TotalScoresResult> getResult() {
        return aggregate();
    }

    private List<TotalScoresResult> aggregate() {
        for (int row = 0; row < deliveries.size(); row++) {
            String key = generateKey(deliveries.get(row));
            Runs value = counter.getOrDefault(key, new Runs());
            value.fours = value.fours + (deliveries.get(row)
                                                   .getBATSMAN_RUNS() == 4 ? 1 : 0);
            value.sixes = value.sixes + (deliveries.get(row)
                                                   .getBATSMAN_RUNS() == 6 ? 1 : 0);
            value.total = value.total + deliveries.get(row)
                                                  .getTOTAL_RUNS();
            counter.put(key, value);
        }
        return counter.entrySet()
                      .stream()
                      .map(elem -> getTotalScoresResult(elem))
                      .collect(Collectors.toList());
    }

    private TotalScoresResult getTotalScoresResult(final Map.Entry<String, Runs> elem) {
        TotalScoresResult result = new TotalScoresResult();
        String[] keys = elem.getKey()
                            .split(KEY_DELIMITER);
        result.setYear(keys[0]);
        result.setTeamName(keys[1]);
        result.setFoursCount(elem.getValue().fours);
        result.setSixesCount(elem.getValue().sixes);
        result.setTotalScore(elem.getValue().total);
        return result;
    }

    private String generateKey(final Deliveries deliveries) {
        return matchSeasonMap.get(deliveries.getMATCH_ID()) + KEY_DELIMITER + deliveries.getBATTING_TEAM();
    }

}
