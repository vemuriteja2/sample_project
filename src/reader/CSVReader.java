package reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {

    private static final String CSV_DELIMITER = ",";

    public List<String[]> read(String csvFile) {
        List<String[]> output = new ArrayList();
        String line = "";
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            //Removing Heading
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] details = line.split(CSV_DELIMITER);
                output.add(details);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return output;
    }

}
