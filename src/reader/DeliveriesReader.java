package reader;

import dao.Deliveries;

import java.util.List;
import java.util.stream.Collectors;

public class DeliveriesReader implements InfoReader<Deliveries> {

    private static final String DELIVERIES_FILE = "deliveries.csv";
    private CSVReader csvReader;

    public DeliveriesReader(CSVReader csvReader) {
        this.csvReader = csvReader;
    }

    @Override
    public List<Deliveries> getInfo() {
        List<String[]> input = csvReader.read(DELIVERIES_FILE);
        return input.stream()
                    .map(row -> Deliveries.fromStringArray(row))
                    .collect(Collectors.toList());
    }
}
