package reader;

import java.util.List;

public interface InfoReader<T> {

    List<T> getInfo();
}
