package reader;

import dao.Matches;

import java.util.List;
import java.util.stream.Collectors;

public class MatchesReader implements InfoReader<Matches> {

    private static final String MATCHES_FILE = "matches.csv";
    private CSVReader csvReader;

    public MatchesReader(CSVReader csvReader) {
        this.csvReader = csvReader;
    }

    @Override
    public List<Matches> getInfo() {
        List<String[]> input = csvReader.read(MATCHES_FILE);
        return input.stream()
                    .map(row -> Matches.fromStringArray(row))
                    .collect(Collectors.toList());
    }
}
